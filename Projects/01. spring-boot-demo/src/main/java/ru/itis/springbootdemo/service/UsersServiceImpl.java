package ru.itis.springbootdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.SignUpDto;
import ru.itis.springbootdemo.dto.UserDto;
import ru.itis.springbootdemo.models.User;
import ru.itis.springbootdemo.repositories.UsersRepository;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

//    @Autowired
//    @Qualifier(value = "costsInputStream")
//    private InputStream inputStream;

    @Override
    public List<UserDto> getAllUsers(Integer page, Integer size, String property) {
        Sort sort = Sort.by(property);
        PageRequest request = PageRequest.of(page, size, sort);
        Page<User> pageResult = usersRepository.findAll(request);
        List<User> users = pageResult.getContent();
        return UserDto.from(users);
    }

    @Override
    public UserDto getUser(Long userId) {
        return UserDto.from(usersRepository.getOne(userId));
    }

    @Override
    public UserDto addUser(SignUpDto userData) {
        User user = User.builder()
                .email(userData.getEmail())
                .password(userData.getPassword())
                .name(userData.getName())
                .createdAt(LocalDateTime.now())
                .build();

        usersRepository.save(user);
        return UserDto.from(user);
    }
}
