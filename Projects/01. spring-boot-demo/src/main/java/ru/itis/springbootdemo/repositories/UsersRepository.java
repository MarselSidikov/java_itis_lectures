package ru.itis.springbootdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.springbootdemo.models.User;

import java.time.LocalDateTime;
import java.util.List;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User>
    findAllByNameContainsAndCreatedAtBeforeOrEmailOrderByIdDesc(String name, LocalDateTime before, String email);
}
