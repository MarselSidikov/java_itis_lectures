package ru.itis.springbootdemo.service;

import ru.itis.springbootdemo.dto.SignUpDto;
import ru.itis.springbootdemo.dto.UserDto;

import java.util.List;

public interface UsersService {
    List<UserDto> getAllUsers(Integer page, Integer size, String sort);

    UserDto getUser(Long userId);

    UserDto addUser(SignUpDto userData);
}
