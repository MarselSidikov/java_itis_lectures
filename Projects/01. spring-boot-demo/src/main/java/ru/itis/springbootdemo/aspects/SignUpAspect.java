package ru.itis.springbootdemo.aspects;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ru.itis.springbootdemo.dto.SignUpDto;

@Aspect
@Component
@Slf4j
public class SignUpAspect {
//    private final Logger log = LoggerFactory.getLogger(SignUpAspect.class);

    @Before(value = "execution(* ru.itis.springbootdemo.service.SignUpService.signUp(*))")
    public void before(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        SignUpDto user = (SignUpDto) args[0];
        log.info("Пользователь - " + user + " начал регистрацию");
    }

    @After(value = "execution(* ru.itis.springbootdemo.service.SignUpService.signUp(*))")
    public void after(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        SignUpDto user = (SignUpDto) args[0];
        log.info("Пользователь - " + user + " завершил регистрацию");
    }
}
