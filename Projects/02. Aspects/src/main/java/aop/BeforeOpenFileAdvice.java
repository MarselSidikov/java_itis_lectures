package aop;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class BeforeOpenFileAdvice implements MethodBeforeAdvice {

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Вызов метода в FileAccessor");
        FileAccessor castedTarget = (FileAccessor) target;
        System.out.println("Объект FileAccessor создан - " + castedTarget.getCreatedObjectTime());
        System.out.println("Происходит открытие файла - " + args[0]);
    }
}
