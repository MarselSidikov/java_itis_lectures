package aop;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class FileAccessor {

    private LocalTime createdObjectTime;

    public FileAccessor() {
        this.createdObjectTime = LocalTime.now();
    }

    public void openFile(String fileName) {
        try {
            InputStream input = new FileInputStream(fileName + "A");
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public LocalTime getCreatedObjectTime() {
        return createdObjectTime;
    }
}
