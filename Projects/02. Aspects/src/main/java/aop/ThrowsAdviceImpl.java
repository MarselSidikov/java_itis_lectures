package aop;

import org.springframework.aop.ThrowsAdvice;

public class ThrowsAdviceImpl implements ThrowsAdvice {
    public void afterThrowing(Exception ex) {
        System.out.println("У ВАС ОШИБКА!!!! ВНИМАНИЕ!!! ЧТО-ТО ПОШЛО НЕ ТАК!!!");
    }
}
