package aop;

import org.springframework.aop.framework.ProxyFactory;

public class Main {
    public static void main(String[] args) {
        FileAccessor target = new FileAccessor();
        ProxyFactory proxyFactory = new ProxyFactory(target);
        BeforeOpenFileAdvice beforeAdvice = new BeforeOpenFileAdvice();
        MethodAdvice methodAdvice = new MethodAdvice();
        ThrowsAdviceImpl throwsAdvice = new ThrowsAdviceImpl();
        proxyFactory.addAdvice(beforeAdvice);
        proxyFactory.addAdvice(methodAdvice);
        proxyFactory.addAdvice(throwsAdvice);
        FileAccessor proxy = (FileAccessor) proxyFactory.getProxy();
        proxy.openFile("input.txt");
    }
}
