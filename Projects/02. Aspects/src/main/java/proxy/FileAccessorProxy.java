package proxy;

public class FileAccessorProxy extends FileAccessor {
    private FileAccessor target;

    private BeforeAdvice beforeAdvice;
    private AfterAdvice afterAdvice;

    public FileAccessorProxy(FileAccessor target) {
        this.target = target;
    }

    @Override
    public void openFile(String fileName) {
        beforeAdvice.before();
        target.openFile(fileName);
        afterAdvice.after();
    }

    public void setBeforeAdvice(BeforeAdvice beforeAdvice) {
        this.beforeAdvice = beforeAdvice;
    }

    public void setAfterAdvice(AfterAdvice afterAdvice) {
        this.afterAdvice = afterAdvice;
    }
}
