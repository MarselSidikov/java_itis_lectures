package proxy;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class FileAccessor {

    public void openFile(String fileName) {
        try {
            InputStream input = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден");
        }
    }
}
