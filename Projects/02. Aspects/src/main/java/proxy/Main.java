package proxy;

public class Main {
    public static void main(String[] args) {
        FileAccessor target = new FileAccessor();

        FileAccessorProxy proxy = new FileAccessorProxy(target);
        proxy.setBeforeAdvice(() -> {
            System.out.println("Открытие файла");
        });

        proxy.setAfterAdvice(() -> {
            System.out.println("Закрыли файл");
        });

        FileAccessor proxied = proxy;
        proxied.openFile("input.txt");

    }
}
