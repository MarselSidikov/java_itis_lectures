package proxy;

public interface BeforeAdvice {
    void before();
}
