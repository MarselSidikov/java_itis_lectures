package proxy;

public interface AfterAdvice {
    void after();
}
